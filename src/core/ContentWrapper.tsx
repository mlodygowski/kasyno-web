import {ApplicationRouter} from '@router/index';
import React from 'react';

const ContentWrapper = () => {
    return (
        <div className="layout-wrapper main-container-style">
            <ApplicationRouter />
        </div>
    );
};

export default ContentWrapper;
