import {core} from './core.epic';
import {auth} from './auth.epic';

export const epics = [...auth, ...core];
