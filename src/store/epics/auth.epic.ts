import {IAction} from '@shared/interfaces';
import {AuthActions, loginSuccess} from '@store/actions';
import {ofType} from 'redux-observable';
import {Observable, switchMap} from 'rxjs';

const onLogin$ = (actions$: Observable<IAction>) =>
    actions$.pipe(
        ofType(AuthActions.LOGIN),
        switchMap((action: IAction) => {
            //api request
            return [loginSuccess('token')];
        }),
    );

export const auth = [onLogin$];
