export const AuthActions = {
    TEST: '[Auth] Test',
    LOGIN: '[Auth] Login',
    LOGIN_SUCCESS: '[Auth] Login success',
    LOGOUT: '[Auth] Logout',
};

export const testAction = (data: string) => ({
    type: AuthActions.TEST,
    data,
});

export const login = (email: string, password: string) => ({
    type: AuthActions.LOGIN,
    data: {email, password},
});

export const loginSuccess = (credentials: string) => ({
    type: AuthActions.LOGIN_SUCCESS,
    data: credentials,
});

export const logout = () => ({
    type: AuthActions.LOGOUT,
});
