import {IAction} from '@shared/interfaces';
import {AuthActions} from '@store/actions';

export interface IAuthState {
    jwt: string | null;
}

const AuthReducer = (state = {}, action: IAction) => {
    switch (action.type) {
        case AuthActions.LOGIN_SUCCESS:
            return {...state, jwt: action.data};
        case AuthActions.LOGOUT:
            return {...state, jwt: null};
        default:
            return state;
    }
};

export default AuthReducer;
