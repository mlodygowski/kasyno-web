import {combineReducers} from 'redux';
import authReducer, {IAuthState} from './auth.reducer';

export interface IAppState {
    auth: IAuthState;
}

const reducers = combineReducers({
    auth: authReducer,
});

export default reducers;
