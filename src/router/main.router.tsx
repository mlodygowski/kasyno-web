import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {LoginPage} from '@pages/auth';

const MainRouter = () => {
    return (
        <Switch>
            <Route exact path="/">
                <Redirect to="/auth" />
            </Route>
            <Route path="/auth">
                <LoginPage />
            </Route>
        </Switch>
    );
};

export default MainRouter;
