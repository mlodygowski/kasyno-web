import React from 'react';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import './login.page.scss';
import {Password} from 'primereact/password';
import {classNames} from 'primereact/utils';
import './login.page.scss';
import {useFormik} from 'formik';
import {useDispatch} from 'react-redux';
import {login} from '@store/actions';

interface ILoginFormData {
    email: string;
    password: string;
}

const LoginPage = () => {
    const dispatch = useDispatch();

    const formik = useFormik<ILoginFormData>({
        initialValues: {
            email: '',
            password: '',
        },
        validate: (data: ILoginFormData) => {
            const errors: Partial<ILoginFormData> = {};

            if (!data.email) {
                errors.email = 'Email is required.';
            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(data.email)) {
                errors.email = 'Invalid email address. E.g. example@email.com';
            }

            if (!data.password) {
                errors.password = 'Password is required.';
            }

            return errors;
        },
        onSubmit: (data: ILoginFormData) => {
            dispatch(login(data.email, data.password));
            formik.resetForm();
        },
    });

    const isFormFieldValid = (name: 'password' | 'email'): boolean =>
        !!(formik.touched[name] && formik.errors[name]);

    const getFormErrorMessage = (name: 'password' | 'email'): JSX.Element | null => {
        return isFormFieldValid(name) ? (
            <small className="p-error">{formik.errors[name]}</small>
        ) : null;
    };

    return (
        <section id="auth" className="main-container">
            <div className={'login-form'}>
                <h4 className="mb-5">Login Form</h4>
                <form onSubmit={formik.handleSubmit} className="p-fluid">
                    <div className="mb-5">
                        <span className="p-float-label p-input-icon-right">
                            <i className="pi pi-envelope" />
                            <InputText
                                id="email"
                                name="email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                className={classNames({'p-invalid': isFormFieldValid('email')})}
                            />
                            <label
                                htmlFor="email"
                                className={classNames({'p-error': isFormFieldValid('email')})}>
                                Email*
                            </label>
                        </span>
                        {getFormErrorMessage('email')}
                    </div>
                    <div>
                        <span className="p-float-label">
                            <Password
                                id="password"
                                name="password"
                                feedback={false}
                                value={formik.values.password}
                                onChange={formik.handleChange}
                                toggleMask
                                className={classNames({'p-invalid': isFormFieldValid('password')})}
                            />
                            <label
                                htmlFor="password"
                                className={classNames({'p-error': isFormFieldValid('password')})}>
                                Password*
                            </label>
                        </span>
                        {getFormErrorMessage('password')}
                    </div>

                    <Button type="submit" label="Login" className="mt-6" />
                </form>
            </div>
        </section>
    );
};

export default LoginPage;
