import {handleSuccess} from '@store/actions';
import {Dispatch} from 'react';

export const menuDefault = (dispatch: Dispatch<any>) => {
    return [
        {
            label: 'Live stream',
            icon: 'pi pi-fw pi-file',
            command: () => {
                dispatch(handleSuccess('Menu', 'Live stream'));
            },
        },
        {
            label: 'Notifications',
            icon: 'pi pi-fw pi-pencil',
            command: () => {
                dispatch(handleSuccess('Menu', 'Notification'));
            },
        },
        {
            label: 'Promos',
            icon: 'pi pi-fw pi-user',
            command: () => {
                dispatch(handleSuccess('Menu', 'Promos'));
            },
        },
        {
            label: 'Jackpot',
            icon: 'pi pi-fw pi-user',
            command: () => {
                dispatch(handleSuccess('Menu', 'Jackpot'));
            },
        },
        {
            label: 'Static content',
            icon: 'pi pi-fw pi-calendar',
            items: [
                {
                    label: 'Terms of use',
                    icon: 'pi pi-fw pi-pencil',
                    command: () => {
                        dispatch(handleSuccess('Menu', 'Static content: terms of use'));
                    },
                },
                {
                    label: 'Privacy policy',
                    icon: 'pi pi-fw pi-calendar-times',
                    command: () => {
                        dispatch(handleSuccess('Menu', 'Static content: Privacy policy'));
                    },
                },
            ],
        },
    ];
};
