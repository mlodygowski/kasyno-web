import {Toast} from 'primereact/toast';
import React from 'react';

abstract class ToastService {
    static toastRef = React.createRef<Toast>();
    static success(title: string, message: string) {
        this.toastRef.current?.show({
            severity: 'success',
            summary: title,
            detail: message,
        });
    }
    static error(err: Record<string, any>) {
        const errors = err.graphQLErrors?.length ? err.graphQLErrors : err.errors;
        this.toastRef.current?.show({
            severity: 'error',
            summary: err.message,
            detail: errors[0],
        });
    }
}
export default ToastService;
