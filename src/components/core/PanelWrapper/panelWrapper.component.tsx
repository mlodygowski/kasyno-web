import {Toolbar} from 'primereact/toolbar';
import React from 'react';
import './panelWrapper.component.scss';
import {TieredMenu} from 'primereact/tieredmenu';
import {MenuItem} from 'primereact/menuitem';

interface IPanelWrapper {
    menuItems: MenuItem[] | undefined;
    children: React.ReactNode;
}

const PanelWrapper = (props: IPanelWrapper) => {
    return (
        <div className="main-container-style">
            <div className="grid right-margin-fix">
                <div className="col-12 toolbar-col">
                    <Toolbar left={<div>Kasyno</div>} />
                </div>
            </div>
            <div className="grid right-margin-fix left-margin-fix main-container-style">
                <div className="col-2 sidemenu-col">
                    <TieredMenu model={props.menuItems} className="main-container-style" />
                </div>
                <div className="col-10 position-relative">{props.children}</div>
            </div>
        </div>
    );
};

export default PanelWrapper;
