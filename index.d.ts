declare module '*.scss';
declare module '*.png';
declare module '*.jpg';

declare module 'kasyno' {
    export const appVersion: string;
}
