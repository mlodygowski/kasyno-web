const CracoAlias = require('craco-alias');
const sassResourcesLoader = require('craco-sass-resources-loader');
const path = require('path');

module.exports = {
    plugins: [
        {
            plugin: CracoAlias,
            options: {
                source: 'tsconfig',
                baseUrl: './',
                tsConfigPath: './tsconfig.paths.json',
            },
        },
        {
            plugin: sassResourcesLoader,
            options: {
                resources: [
                    path.resolve(__dirname, './src/App.scss'),
                    path.resolve(__dirname, './src/layout/_variables.scss'),
                ],
            },
        },
    ],
};
